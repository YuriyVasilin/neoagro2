class Form extends React.Component {
    constructor(props) {
        super(props);
        
        let fio = window.localStorage.getItem("fio") || "";
        let phone = window.localStorage.getItem("phone") || "";
        let region = window.localStorage.getItem("region") || "";
        let message = window.localStorage.getItem("message") || "";

        this.state = {
            fio: fio,
            phone: phone,
            region: region,
            message: message
        }
        
        this.changeFIO = this.changeFIO.bind(this);
        this.changePhone = this.changePhone.bind(this);
        this.changeMessage = this.changeMessage.bind(this);
        this.changeRegion = this.changeRegion.bind(this);
        this.send = this.send.bind(this);
        this.resetForm = this.resetForm.bind(this);
    }
    
    changeFIO(event) {
        this.setState({
            fio: event.target.value
        });
        window.localStorage.setItem("fio", event.target.value);
    }
    
    changePhone(event) {
        this.setState({
            phone: event.target.value
        });
        window.localStorage.setItem("phone", event.target.value);
    }
    
    changeRegion(event) {
        this.setState({
            region: event.target.value
        });
        window.localStorage.setItem("region", event.target.value);
    }
    
    changeMessage(event) {
        this.setState({
            message: event.target.value
        });
        window.localStorage.setItem("message", event.target.value);
    }
    
    resetForm() {
        window.localStorage.setItem("fio", "");
        window.localStorage.setItem("phone", "");
        window.localStorage.setItem("region", "");
        window.localStorage.setItem("message", "");
        this.setState({
            fio: "",
            phone: "",
            region: "",
            message: ""
        });
        CallUs.reset();
        history.back();
    }
    
    send() {
            document.getElementById('sendBtn').disabled = true;
            fetch("https://api.slapform.com/yvasilin@gmail.com", {
                method: "POST",
                body: JSON.stringify(this.state),
                headers: {
                    "Content-Type": "application/json"
                },
            }).then(
                result => {
                alert("Мы скоро свяжемся с вами!");
                document.getElementById('sendBtn').disabled = false;
                this.resetForm();
                },
                error => {
                    document.getElementById('sendBtn').disabled = false;
                    let repeat = confirm("Ошибка! Повторить отправку?");
                    if (repeat) this.send();
                }
            )
    }

    render() {
        return (
                <form name="CallUs" id="CallUs" acceptCharset="UTF-8">
                    <div><input id="fio" className="inputPopup1 grey" type="text" value={this.state.fio} onChange={this.changeFIO} placeholder="ФИО" required /></div>
                    <div><input id="phone" className="inputPopup1 grey" type="tel" value={this.state.phone} onChange={this.changePhone} placeholder="Телефон" required /></div>
                    <select id="region" value={this.state.region} onChange={this.changeRegion} placeholder="Телефон" className="inputPopup2" required>
                        <option value="" disabled>Регион</option>
                        <option value="Республика Адыгея">Республика Адыгея</option>
                        <option value="Республика Башкортостан">Республика Башкортостан</option>
                        <option value="Республика Бурятия">Республика Бурятия</option>
                        <option value="Республика Алтай">Республика Алтай</option>
                        <option value="Республика Дагестан">Республика Дагестан</option>
                        <option value="Республика Ингушетия">Республика Ингушетия</option>
                        <option value="Кабардино-Балкарская Республика">Кабардино-Балкарская Республика</option>
                        <option value="Республика Калмыкия">Республика Калмыкия</option>
                        <option value="Карачаево-Черкесская Республика">Карачаево-Черкесская Республика</option>
                        <option value="Республика Карелия">Республика Карелия</option>
                        <option value="Республика Коми">Республика Коми</option>
                        <option value="Республика Марий Эл">Республика Марий Эл</option>
                        <option value="Республика Мордовия">Республика Мордовия</option>
                        <option value="Республика Саха">Республика Саха</option>
                        <option value="Республика Северная Осетия - Алания">Республика Северная Осетия - Алания</option>
                        <option value="Республика Татарстан">Республика Татарстан</option>
                        <option value="Республика Тыва">Республика Тыва</option>
                        <option value="Удмуртская Республика">Удмуртская Республика</option>
                        <option value="Республика Хакасия">Республика Хакасия</option>
                        <option value="Чеченская Республика">Чеченская Республика</option>
                        <option value="Чувашская Республика">Чувашская Республика</option>
                        <option value="Алтайский край">Алтайский край</option>
                        <option value="Краснодарский край">Краснодарский край</option>
                        <option value="Красноярский край">Красноярский край</option>
                        <option value="Приморский край">Приморский край</option>
                        <option value="Ставропольский край">Ставропольский край</option>
                        <option value="Хабаровский край">Хабаровский край</option>
                    </select>
                    <div><textarea className="inputPopup3 grey" id="mess" value={this.state.message} onChange={this.changeMessage} placeholder="Cообщение"></textarea></div>
                    <div><button id="sendBtn" className="btn-main" type="button" onClick={this.send}>Отправить</button></div>
                    <button title="Закрыть" type="button" className="closeButton">×</button>
                </form>
        );
    }
}

ReactDOM.render(<Form />, document.getElementById("callingUs"));